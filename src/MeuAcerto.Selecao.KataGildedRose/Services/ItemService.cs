﻿using MeuAcerto.Selecao.KataGildedRose.Repositories;
using MeuAcerto.Selecao.KataGildedRose.Repositories.Interfaces;
using MeuAcerto.Selecao.KataGildedRose.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Services
{
    public class ItemService : IItemService
    {
        private readonly IItemRepository repository;
        public ItemService ()
        {
            repository = new ItemRepository();
        }
        public void NextDay(ICollection<Item> itens)
        {
            repository.NextDay(itens);
            
        }
        public void NextDayOneItem(Item item)
        {
            repository.NextDayOneItem(item);
        }
    }
}
