﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Services.Interfaces
{
    public interface IItemService
    {
        void NextDay(ICollection<Item> itens);
        void NextDayOneItem(Item item);
    }
}
