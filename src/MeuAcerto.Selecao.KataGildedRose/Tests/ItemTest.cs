﻿using MeuAcerto.Selecao.KataGildedRose.Services;
using MeuAcerto.Selecao.KataGildedRose.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace MeuAcerto.Selecao.KataGildedRose.Tests
{
    public class ItemTest
    {
        public readonly IItemService service = new ItemService();

        [Fact]
        public void Item_DiminuirDeveDuasVezesDepoisDoPrazo()
        {
            const int PrazoDeValidade = 0;
            const int QualidadeDoItem = 10;

            Item item = new Item
            {
                Nome = "Teste",
                PrazoValidade = PrazoDeValidade,
                Qualidade = QualidadeDoItem
            };

            service.NextDayOneItem(item);

            Assert.Equal(QualidadeDoItem - 2, item.Qualidade);
        }

        [Fact]
        public void Item_QualidadeNaoNegativa()
        {
            const int PrazoDeValidade = -1;
            const int QualidadeDoItem = 0;

            Item item = new Item
            {
                Nome = "Teste",
                PrazoValidade = PrazoDeValidade,
                Qualidade = QualidadeDoItem
            };

            service.NextDayOneItem(item);

            Assert.Equal(QualidadeDoItem, item.Qualidade);
        }

        [Theory]
        [InlineData("Queijo Brie Envelhecido")]
        public void Item_QualidadeNaoMaiorQueCinquenta(string itemNome)
        {
            const int PrazoDeValidade = 10;
            const int QualidadeDoItem = 50;

            Item item = new Item
            {
                Nome = itemNome,
                PrazoValidade = PrazoDeValidade,
                Qualidade = QualidadeDoItem
            };

            service.NextDayOneItem(item);

            Assert.Equal(QualidadeDoItem, item.Qualidade);
        }

        [Theory]
        [InlineData("Queijo Brie Envelhecido")]
        public void Item_QueijoDeveAumentarQualidade(string itemNome)
        {
            const int PrazoDeValidade = 10;
            const int QualidadeDoItem = 6;

            Item item = new Item
            {
                Nome = itemNome,
                PrazoValidade = PrazoDeValidade,
                Qualidade = QualidadeDoItem
            };

            service.NextDayOneItem(item);

            Assert.Equal(QualidadeDoItem + 1, item.Qualidade);
        }

        [Theory]
        [InlineData("Sulfuras, a Mão de Ragnaros")]
        public void Item_NãoDeveAlterar(string itemNome)
        {
            const int PrazoDeValidade = 10;
            const int QualidadeDoItem = 80;

            Item item = new Item
            {
                Nome = itemNome,
                PrazoValidade = PrazoDeValidade,
                Qualidade = QualidadeDoItem
            };

            Item itemDefault = new Item
            {
                Nome = itemNome,
                PrazoValidade = PrazoDeValidade,
                Qualidade = QualidadeDoItem
            };

            service.NextDayOneItem(item);

            Assert.Equal(
                new { Qualidade = QualidadeDoItem, PrazoValidade = PrazoDeValidade},
                new { item.Qualidade, item.PrazoValidade }
            );
        }

        [Theory]
        [InlineData("Ingressos para o concerto do TAFKAL80ETC")]
        public void Item_IngressosQualidadeAumentarDois(string itemNome)
        {
            const int PrazoDeValidade = 10;
            const int QualidadeDoItem = 15;

            Item item = new Item
            {
                Nome = itemNome,
                PrazoValidade = PrazoDeValidade,
                Qualidade = QualidadeDoItem
            };

            service.NextDayOneItem(item);

            Assert.Equal(QualidadeDoItem + 2, item.Qualidade);
        }
        [Theory]
        [InlineData("Ingressos para o concerto do TAFKAL80ETC")]
        public void Item_IngressosQualidadeAumentarTres(string itemNome)
        {
            const int PrazoDeValidade = 5;
            const int QualidadeDoItem = 15;

            Item item = new Item
            {
                Nome = itemNome,
                PrazoValidade = PrazoDeValidade,
                Qualidade = QualidadeDoItem
            };

            service.NextDayOneItem(item);

            Assert.Equal(QualidadeDoItem + 3, item.Qualidade);
        }

        [Theory]
        [InlineData("Ingressos para o concerto do TAFKAL80ETC")]
        public void Item_IngressosQualidadeDeveZerar(string itemNome)
        {
            const int PrazoDeValidade = 0;
            const int QualidadeDoItem = 15;

            Item item = new Item
            {
                Nome = itemNome,
                PrazoValidade = PrazoDeValidade,
                Qualidade = QualidadeDoItem
            };

            service.NextDayOneItem(item);

            Assert.Equal(QualidadeDoItem - 15, item.Qualidade);
        }
    }
}
