﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Repositories.Interfaces
{
    public interface IItemRepository
    {
        void NextDay(ICollection<Item> itens);
        void NextDayOneItem(Item item);
    }
}
