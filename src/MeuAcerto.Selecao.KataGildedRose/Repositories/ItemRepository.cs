﻿using MeuAcerto.Selecao.KataGildedRose.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace MeuAcerto.Selecao.KataGildedRose.Repositories
{
    public class ItemRepository : IItemRepository
    {
        public void NextDay(ICollection<Item> itens)
        {
            try
            {
                foreach (var item in itens)
                {
                    if (item.Nome.ToUpperInvariant() == "QUEIJO BRIE ENVELHECIDO")
                    {
                        NextDayQueijo(item);
                    }
                    else if (item.Nome.ToUpperInvariant() == "INGRESSOS PARA O CONCERTO DO TAFKAL80ETC")
                    {
                        NextDayIngresso(item);
                    }
                    else if (item.Nome.ToUpperInvariant().Contains("CONJURADO"))
                    {
                        NextDayConjurado(item);
                    }
                    else if (item.Nome.ToUpperInvariant() != "SULFURAS, A MÃO DE RAGNAROS")
                    {
                        NextDay(item);
                    }
                }
                
            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }
        public void NextDayOneItem(Item item)
        {
            try
            {
                if (item.Nome.ToUpperInvariant() == "QUEIJO BRIE ENVELHECIDO")
                {
                    NextDayQueijo(item);
                }
                else if (item.Nome.ToUpperInvariant() == "INGRESSOS PARA O CONCERTO DO TAFKAL80ETC")
                {
                    NextDayIngresso(item);
                }
                else if (item.Nome.ToUpperInvariant().Contains("CONJURADO"))
                {
                    NextDayConjurado(item);
                }
                else if (item.Nome.ToUpperInvariant() != "SULFURAS, A MÃO DE RAGNAROS")
                {
                    NextDay(item);
                }

            }
            catch (Exception ex)
            {

                throw new Exception(ex.Message);
            }
        }

        #region [Métodos Privados]
        private void NextDay(Item item)
        {
            if (item.PrazoValidade > 0)
                item.SetQualidade(1, true);
            else
                item.SetQualidade(2, true);
            item.DiminuirPrazo();
        }

        private void NextDayConjurado(Item item)
        {
            if (item.PrazoValidade < 0)
                item.SetQualidadeZero();
            else
                item.SetQualidade(2, true);
            item.DiminuirPrazo();
        }
        private void NextDayQueijo(Item item)
        {
            if (item.PrazoValidade > 0)
                item.SetQualidade(1, false);
            else
                item.SetQualidade(2, false);
            item.DiminuirPrazo();
        }
        private void NextDayIngresso(Item item)
        {
            if (item.PrazoValidade >= 11)
            {
                item.SetQualidade(1, false);
            }
            else if (item.PrazoValidade >= 6)
            {
                item.SetQualidade(2, false);
            }
            else if (item.PrazoValidade > 0)
            {
                item.SetQualidade(3, false);
            }
            else
            {
                item.SetQualidadeZero();
            }

            item.DiminuirPrazo();
        }
        #endregion

    }
}
