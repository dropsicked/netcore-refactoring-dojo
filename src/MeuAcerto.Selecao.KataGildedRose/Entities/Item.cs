﻿namespace MeuAcerto.Selecao.KataGildedRose
{
    public sealed class Item
    {
        public string Nome { get; set; }

        public int PrazoValidade { get; set; }

        public int Qualidade { get; set; }

        public void SetQualidade(int quantidade, bool diminui)
        {
            if(diminui)
            {
                if (Qualidade - quantidade >= 0)
                    Qualidade -= quantidade;
            }
            else
            {
                if (Qualidade + quantidade <= 50)
                    Qualidade += quantidade;
                else
                    Qualidade = 50;
            }
            
        }

        public void SetQualidadeZero()
        {
            Qualidade = 0;
        }

        public void DiminuirPrazo()
        {
            PrazoValidade--;
        }
    }

}
