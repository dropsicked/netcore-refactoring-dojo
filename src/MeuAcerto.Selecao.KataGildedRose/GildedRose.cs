﻿using MeuAcerto.Selecao.KataGildedRose.Services;
using MeuAcerto.Selecao.KataGildedRose.Services.Interfaces;
using System.Collections.Generic;

namespace MeuAcerto.Selecao.KataGildedRose
{
    public class GildedRose
    {
        private readonly IList<Item> itens;
        private readonly IItemService service;
        public GildedRose(IList<Item> _itens)
        {
            itens = _itens;
            service = new ItemService();
        }

        public void AtualizarQualidade()
        {
            service.NextDay(itens);
        }
    }
}
